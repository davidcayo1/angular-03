import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculo',
  templateUrl: './calculo.component.html',
  styleUrls: ['./calculo.component.css']
})
export class CalculoComponent implements OnInit {

  number1!: any;
  number2!: any;
  result!: any;

  constructor() { }

  ngOnInit(): void {
  }

  add() {
    this.result = parseInt(this.number1) + parseInt(this.number2); 
  }

}
